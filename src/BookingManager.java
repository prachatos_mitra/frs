import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
/**
 * Implements Booking logic to maintain booked flight records
 * @author Rajarshi Basu, Ronit Ray
 */
public class BookingManager{
    private DataManager dm;
    private FRSManager mgr;
    /**
     * Constructor to link with FRSManager
     * @param mgr Initial class constructor
     */
    public BookingManager(FRSManager mgr){
        this.mgr = mgr;
        dm = mgr.getDataMgr();
    }
    /**
     * Generates a 6 digit UUID to distinguish Booking records
     * @return 6 digit UUID
     */
    private String generatePNR(){
        return java.util.UUID.randomUUID().toString().substring(0,6).toUpperCase();
    }
    /**
     * Writes current booked flight records in to file
     * @param f ComboFlight booked
     * @param name Name booked under
     * @return Latest flight booked entry
     */
    public Booking bookFlight(ComboFlight f, String name) {
        Booking entry = new Booking(generatePNR(),f.getPassNo(),name,f);
        java.util.List<Booking> records = dm.readBooking(); //get all bookings
        records.add(entry); //add current entry
        FileOutputStream fout = null;
        try{
            fout = new FileOutputStream("flights.ser");
            try (ObjectOutputStream oos = new ObjectOutputStream(fout)) {
                oos.writeObject(records); //overwriting file
            } 
            fout.close();
        }
        catch(FileNotFoundException e){
            LogManager.writeToLog(e.getMessage(), System.currentTimeMillis(), "warning.log");
        }
        catch(IOException e){
            LogManager.writeToLog(e.getMessage(), System.currentTimeMillis(), "warning.log");
        }
        return entry;
    }
}