
import java.util.Date;

/**
 * Placeholder class for UI design
 * @author Prachatos Mitra
 */
public interface UIManager {
    /**
     * Displays valid flights in a GUI/CLI context
     * @param from Origin city
     * @param noOfPass Number of passengers
     * @param depDate Date of departure
     */
    public void showSearchResults(String from, int noOfPass, Date depDate);
    /**
     * Displays flight selection details in a GUI/CLI context
     * @param selFlight Selected combo flight
     * @param name Booking name
     */
    public void showConfirmationScreen(ComboFlight selFlight, String name);
    /**
     * Displays flight booking confirmation message in a GUI/CLI context
     * @param selectedFlight Selected combo flight
     */
    public void showBookingScreen(ComboFlight selectedFlight);
    /**
     * Displays the initial input screen in a GUI/CLI context
     */
    public void showInputScreen();
    
    /**
     * Utilized to turn off display and dispose unnecessary objects
     */
    public void displayOff();
}
