
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;

/**
 * Acts as a controller and dispatcher class for Swing UI classes
 * @author Prachatos Mitra, Parmartha Saha
 */
public class GraphicalUIManager implements UIManager{
    private InputScreen uimInpSc;
    private BookingScreen uimBookSc;
    private SearchResults uimSearchR;
    private ConfirmationScreen uimConfSc;
    private FRSManager mgr;
    
    /**
     * Constructor for Swing based UI Manager
     * @param mgr Base FRSManager object
     */
    GraphicalUIManager(FRSManager mgr){
        this.mgr = mgr;
        uimBookSc = new BookingScreen(this);
        uimInpSc = new InputScreen(this);
        uimSearchR = new SearchResults(this);
        uimConfSc = new ConfirmationScreen(this);
    }
   
    /**
     * Shows the input screen
     */
    @Override
    public void showInputScreen(){
        uimInpSc.setVisible(true);
        uimBookSc.setVisible(false);
        uimConfSc.setVisible(false);
        uimSearchR.setVisible(false);      
    }
    
    /**
     * Shows search results screen
     * Overloaded method in case data has already been loaded
     */
    public void showSearchResults() {       
        uimBookSc.setVisible(false);
        uimInpSc.setVisible(false);
        uimConfSc.setVisible(false);
        uimSearchR.setVisible(true);
    }
    
    /**
     * Displays search results
     *
     * @param from Origin city
     * @param noOfPass Number of passengers
     * @param depDate Date of departure from originating city
     */
    @Override  
    public void showSearchResults(String from, int noOfPass, Date depDate){
        //hopefully data was validated in the Swing class
        //FRSManager should handle the actual work and return an array
        List<ComboFlight> searchResults;
        searchResults = mgr.invokeSearch(from, noOfPass, depDate);
        if (searchResults.isEmpty()) {
            JOptionPane.showMessageDialog(uimInpSc, "No matching flights found!", "Error", JOptionPane.ERROR_MESSAGE);
            showInputScreen();
            return;
        }
        //update SearchResults
        uimSearchR.addSearchResult(searchResults);
        //ok we can display
        uimBookSc.setVisible(false);
        uimInpSc.setVisible(false);
        uimConfSc.setVisible(false);
        uimSearchR.setVisible(true);
    }
    
    /**
     * Shows the booking screen
     * @param selectedFlight F
     */
    @Override
    public void showBookingScreen(ComboFlight selectedFlight){
        uimBookSc.setFlight(selectedFlight);
        uimBookSc.setVisible(true);
        uimInpSc.setVisible(false);
        uimConfSc.setVisible(false);
        uimSearchR.setVisible(false);
    }
    
    /**
     * Shows the booked flight and booking details
     *
     * @param selFlight Flight selected from search results
     * @param name Booking name
     */
    @Override
    public void showConfirmationScreen(ComboFlight selFlight, String name){
        //call FRS
        Booking myBooking = mgr.invokeBooking(selFlight, name);
        uimConfSc.showBooking(myBooking);
        //display
        uimBookSc.setVisible(false);
        uimInpSc.setVisible(false);
        uimConfSc.setVisible(true);
        uimSearchR.setVisible(false);
    }
    
    /**
     * Utility function to dispose all JFrames used by the manager
     */
    public void displayOff() {  
        uimBookSc.dispose();
        uimInpSc.dispose();
        uimConfSc.dispose();
        uimSearchR.dispose();
    }
}
