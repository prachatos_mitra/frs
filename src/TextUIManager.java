
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

/**
 * Defines methods used in the Command Line Interface version of the Flight
 * Reservation System
 *
 * @author Prachatos Mitra, Ronit Ray
 */
public class TextUIManager implements UIManager {

    private Scanner sc;
    private FRSManager mgr;
    private int isOver;

    /**
     * Initializes the UIManagerCLI
     *
     * @param mgr Initial class constructor
     */
    public TextUIManager(FRSManager mgr) {
        this.mgr = mgr;
        sc = new Scanner(System.in);
    }

    /**
     * Shows the input screen. Contains control loop.
     */
    @Override
    public void showInputScreen() {
        boolean chk = true;
        while (chk) {
            DateFormat date = new SimpleDateFormat("dd MMM yyyy");
            Date d , r1 = null, r2 = null;
            String s = "", from = "";
            System.out.println("\n\n\t\t\tWelcome to Flight Reservation System");
            System.out.println("\nEnter date of travel (dd MMM yyyy)\nValid dates between 01 OCT 2016 - 13 NOV 2016 (both inclusive)");
            do {
                d=null;
                s = sc.nextLine();
                //try date validation
                try {
                    d = date.parse(s);
                    r1 = date.parse("01 OCT 2016");
                    r2 = date.parse("13 NOV 2016");
                } catch (ParseException e) {
                    System.out.println(e.getMessage());
                }
                if (d != null && (r1.getTime() > d.getTime() || d.getTime() > r2.getTime()))
                    System.out.println("Date not in range. Enter again");
            } while (d == null || r1.getTime() > d.getTime() || d.getTime() > r2.getTime());

            System.out.println("Enter source city (1:Delhi, 2:Mumbai, 3:Pune)");
            int n, t;
            do {
                n = sc.nextInt();
                //validate city choice
                if (n < 1 || n > 3)
                    System.out.println("Incorrect choice. Enter again");
            } while (n < 1 || n > 3);
            if (n == 1) from = "DELHI";
            if (n == 2) from = "MUMBAI";
            if (n == 3) from = "PUNE";
            System.out.println("Destination city is Singapore");
            System.out.println("Enter no. of tickets (1-10)");
            do {
                t = sc.nextInt();
                if (t < 1 || t > 10)
                    System.out.println("Incorrect choice. Enter again");
            } while (t < 1 || t > 10);
            System.out.println("\n\nDate=" + s + "\tOrigin city=" + from + "\tNo of passengers=" + t + "\n\nList of available flights:\n");
            showSearchResults(from, t, d);
            int b, ch;
            b = isOver;
            switch (b) {
                //allow to loop
                case -1:
                    System.out.println("Try again? 1:Yes 2:No");
                    do {
                        ch = sc.nextInt();
                        if (ch < 1 || ch > 2)
                            System.out.println("Incorrect choice. Enter again");
                    } while (ch < 1 || ch > 2);
                    if (sc.hasNextLine()) sc.nextLine();
                    chk = (ch == 1) ? chk : !chk;
                    break;
                case 1:
                    chk = !chk;
                    break;
            }
        }
    }

    /**
     * Displays search results
     *
     * @param from Origin city
     * @param t Number of passengers
     * @param d Date of departure from originating city
     */
    @Override
    public void showSearchResults(String from, int t, Date d) {
        List<ComboFlight> src = mgr.invokeSearch(from, t, d);
        if (src.isEmpty()) {
            System.out.println("No flight available");
            isOver = -1;
            return;
        }
        for (int i = 0; i < src.size(); i++) {
            ComboFlight obj = src.get(i);
            //display relevant data for ith result
            String time = (obj.time() / 60) + " hrs, " + (obj.time() % 60) + " mins";
            String ltime=(obj.layover() / 60) + " hrs, " + (obj.layover() % 60) + " mins";
            System.out.println((i + 1) + ": Intermediate city: " + obj.getInter() +
                    "\nTotal time= " + time + " Layover time= " + ltime + "\nFlight numbers= " + obj.getFno() + 
                    "\nTimings: " + obj.getDepDate()+ " " + obj.getSpiceDep() + " (IST) to " + 
                    obj.getArvDate() + " " + obj.getSilkArv() + " (SGT)\n");
        }
        System.out.println("\n\nEnter number to book flight (1-" + (src.size()) + ")");
        System.out.println("Enter 0 to start over");
        int x;
        do {
            x = sc.nextInt();
            if (x < 0 || x > src.size())  System.out.println("Incorrect choice. Enter again");
        } while (x < 0 || x > src.size());
        if (x == 0) isOver = 0;
        else showBookingScreen(src.get(x - 1));
    }

    /**
     * Books a flight against a passenger's name
     *
     * @param selFlight Flight selected from search results
     */
    @Override
    public void showBookingScreen(ComboFlight selFlight) {
        if(sc.hasNextLine()) sc.nextLine();
        System.out.println("Enter booking name");
        String nm = sc.nextLine();
        Booking bm = mgr.invokeBooking(selFlight, nm);
        showBooking(bm);
        showConfirmationScreen(selFlight, nm);
    }

    /**
     * Shows the booked flight and booking details
     *
     * @param selFlight Flight selected from search results
     * @param name Booking name
     */
    @Override
    public void showConfirmationScreen(ComboFlight selFlight, String name) {
        System.out.println("Book another? 1:Yes 2:No");
        int x = sc.nextInt();
        if(sc.hasNextLine()) sc.nextLine();
        isOver = x - 1;
    }
    /**
     * Displays the booked flight and booking details
     * @param bm Booking object for current booking
     */
    public void showBooking(Booking bm){
        ComboFlight cf=bm.getBooking();
        //pretty paint booking info
        System.out.println("\nBooking successful\n"
                + "\t\t\tBOOKING DETAILS"+"\n\tBooking ID : "+bm.getPNR()+"\n\tBooking name : "+bm.getPassName()+
                "\n\tNumber of passengers : "+bm.getPassNo());
        System.out.println("\n\n\t\t\tFLIGHT DETAILS"+"\n\tFlight number : "+cf.getFno().substring(0, 8)+
                "\n\tDate & time of departure : "+cf.getDepDate()+ " " + cf.getSpiceDep() + "\n\tFrom : " + cf.getOrig() + 
                "\n\tTo : "+cf.getInter());
        System.out.println("\n\n\t\t\tCONNECTING FLIGHT DETAILS"+"\n\tFlight number : "+cf.getFno().substring(8)+
                "\n\tDate & time of departure : "+cf.getInterDate()+ " " + cf.getSilkDep() + 
                "\n\tDate & time of arrival: " + cf.getArvDate() + " " + cf.getSilkArv() +
                "\n\tFrom : " + cf.getInter() + 
                "\n\tTo : SINGAPORE");
    }
    
    /**
     * Function to turn display off
     * Unused here as no extra handling is required
     */
    @Override
    public void displayOff() {
        
    }
}