import java.util.Date;
/**
 * Links a Flight object with a Date and the number of passengers
 * @author Ronit Ray
 */
public class ScheduledFlight extends Flight implements java.io.Serializable{
    private Date flightDate;
    private int pass;
    public static int MAX_SEATS = 15;
    /**
     * Initializes a ScheduledFlight object with the particular Flight, date of departure and number of passengers
     * @param f Flight object - may be SpiceJet or SilkAir
     * @param date Date of departure
     * @param p Number of passengers
     */
    public ScheduledFlight(Flight f, Date date,int p){
        super(f);
        flightDate = date;
        pass = p;
    }
    /**
     * Getter method for date of departure
     * @return Date of departure
     */
    public String getDate(){//returns MMM dd yyyy
        String arr[] = flightDate.toString().split(" ");
        return arr[1]+" "+arr[2]+" "+arr[arr.length-1];
    }
    /**
     * Getter method for time of arrival
     * @return Time of arrival
     */
    public String getArv(){
        String s[] = arvTime.toString().split(" ");
        return s[3];
    }
    /**
     * Getter method for time of departure
     * @return Time of departure
     */
    public String getDep(){
        String s[] = depTime.toString().split(" ");
        return s[3];
    }
    /**
     * Getter method for number of passengers on this flight
     * @return Number of passengers
     */
    public int getPass(){
        return pass;
    }
}