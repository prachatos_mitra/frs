
import java.util.Date;
import java.util.List;

/**
 * Initial class for Flight Reservation System
 *
 * @author Prachatos Mitra
 */
public class FRSManager {

    private UIManager frsUIMgr;
    private DataManager frsDataMgr;
    private BookingManager frsBookingMgr;
    private SearchManager frsSearchMgr;
    /**
     * SpiceJet flight database
     */
    protected List<Flight> flightSpiceJet;
    /**
     * SilkAir flight database
     */
    protected List<Flight> flightSilkAir;

    /**
     * Initializes the UI type
     *
     * @param spiceFile File containing SpiceJet details
     * @param silkFile File containing SilkAir details
     * @param uiType Determines UI type: GUI/CLI
     */
    public FRSManager(String spiceFile, String silkFile, String uiType) {
        frsUIMgr = selectUI(uiType);
        //DataMgr will handle error in case file not found
        frsDataMgr = new DataManager(this, spiceFile, silkFile);
        frsBookingMgr = new BookingManager(this);
        frsSearchMgr = new SearchManager(this);
    }
    
    /**
     * Method to select appropriate UI from command line parameter
     * Possible choices are text and Swing
     * @param uiType Parameter containing UI Type. Default action is loading Swing.
     * @return An instance of the UI type requested
     */
    private UIManager selectUI(String uiType) {
        if (uiType.equalsIgnoreCase("text")) {
            return new TextUIManager(this);
        } else if (uiType.isEmpty()) {
            return new GraphicalUIManager(this);
        } else {
            LogManager.writeToLog("Display " + uiType + " not implemented, defaulting to Swing Display", System.currentTimeMillis(), "warning.log");
            return new GraphicalUIManager(this);
        }
    }
    
    /**
     * Returns the DataManager instance for the current FRSManager instance
     *
     * @return DataManager instance
     */
    public DataManager getDataMgr() {
        return this.frsDataMgr;
    }

    /**
     * Initializes the flight databases
     * @return Boolean True if both arrays are not empty
     */
    public Boolean instantiateData() {
        this.flightSpiceJet = this.frsDataMgr.getSpiceJet();
        this.flightSilkAir = this.frsDataMgr.getSilkAir();
        return !(flightSpiceJet.isEmpty() || flightSilkAir.isEmpty());
    }

    /**
     * Prepares the GUI/CLI
     */
    public void instantiateUI() {
        this.frsUIMgr.showInputScreen();
    }

    /**
     * Returns the valid list of connecting flights after search
     *
     * @param from Origin city
     * @param noOfPass Number of passengers
     * @param depDate Date of departure
     * @return ComboFlight list
     */
    public List<ComboFlight> invokeSearch(String from, int noOfPass, Date depDate) {
        return this.frsSearchMgr.search(depDate, from, noOfPass);
    }

    /**
     * Prepares an entry to be written to booking records
     *
     * @param selFlight ComboFlight booked
     * @param name Name of booking
     * @return the Booking object prepared
     */
    public Booking invokeBooking(ComboFlight selFlight, String name) {
        return this.frsBookingMgr.bookFlight(selFlight, name);
    }
    
    /**
     * Main method The main method performs the instantiation steps
     *
     * @param args Used for deciding UI type
     */
    public static void main(String args[]) {
        FRSManager myMgr;
        String silkFile = "", spiceFile = "", uiType = "";
        //Use try catch block to pass as many parameters as possible
        try {
            spiceFile = args[0].trim();
            silkFile = args[1].trim();
            uiType = args[2].trim();
        } catch (Exception e) {
            LogManager.writeToLog("Empty parameter list in main()", System.currentTimeMillis(), "warning.log");
        }
        myMgr = new FRSManager(spiceFile, silkFile, uiType);
        //If we can't read data, no need to proceed
        Boolean dbValid = myMgr.instantiateData();
        if (!dbValid) {
            System.out.println("Could not read databases. Please ensure filenames are correct!");           
            myMgr.frsUIMgr.displayOff();
        } else {
            myMgr.instantiateUI();
        }
    }
}