
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This class creates a ComboFlight object which contains two ScheduledFlight objects for connecting any primary
 * location to Singapore, via an intermediate city.
 * It also contains the number of passengers for all bookings done for this particular combination
 * of flights.
 * @author Ronit Ray, Prachatos Mitra
 */
public class ComboFlight implements java.io.Serializable {

    private ScheduledFlight spiceJet;
    private ScheduledFlight silkAir;
    private int numPass;
    private Date inter;

    /**
     * Initializes the ComboFlight object
     * @param sp Scheduled flight for SpiceJet
     * @param si Scheduled flight for SilkAir
     * @param i Date when interconnecting city is reached
     * @param n Number of passengers
     */
    public ComboFlight(ScheduledFlight sp, ScheduledFlight si, Date i, int n) {
        spiceJet = sp;
        silkAir = si;
        numPass = n;
        inter=i;
    }

    /**
     * Getter method for obtaining SpiceJet flight details
     * @return SpiceJet flight details
     */
    public ScheduledFlight getSpice() {
        return spiceJet;
    }

    /**
     *Getter method for obtaining SilkAir flight details
     * @return SilkAIr flight details
     */
    public ScheduledFlight getSilk() {
        return silkAir;
    }

    /**
     * Getter method for number of passengers in this flight
     * @return Number of passengers
     */
    public int getPassNo() {
        return numPass;
    }

    /**
     * Getter method for SpiceJet flight arrival time
     * @return SpiceJet flight arrival time
     */
    public String getSpiceArv() {
        return spiceJet.getArv();
    }

    /**
     * Getter method for SpiceJet flight departure time
     * @return SpiceJet flight departure time
     */
    public String getSpiceDep() {
        return spiceJet.getDep();
    }

    /**
     * Getter method for SilkAir flight arrival time
     * @return SilkAir flight arrival time
     */
    public String getSilkArv() {//SGT Time
        SimpleDateFormat time = new SimpleDateFormat("HH:mm:ss");
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(time.parse(silkAir.getArv()));
        } catch (java.text.ParseException e) {
            LogManager.writeToLog(e.getMessage(), System.currentTimeMillis(), "error.log");
        }
        c.add(Calendar.MINUTE, 2 * 60 + 30);
        return time.format(c.getTime());
    }

    /**
     * Getter method for SilkAir flight departure time
     * @return SilkAir flight departure time
     */
    public String getSilkDep() {
        return silkAir.getDep();
    }

    /**
     * Getter method for interconnecting city name
     * @return Interconnecting city name
     */
    public String getInter() {
        return spiceJet.dest;
    }

    /**
     * Getter method for origin city
     * @return Name of origin city
     */
    public String getOrig() {
        return spiceJet.orig;
    }

    /**
     * Getter method for flight number
     * @return Flight number
     */
    public String getFno() {
        return spiceJet.getFno() + " " + silkAir.getFno();
    }

    /**
     * Getter method for SpiceJet flight date of departure
     * @return SpiceJet flight date of departure
     */
    public String getDepDate() {
        return spiceJet.getDate();
    }
    
    /**
     * Getter method for SilkAir flight date of departure
     * @return SilkAir flight date of departure
     */
    public String getInterDate(){
        String arr[] = inter.toString().split(" ");
        return arr[1]+" "+arr[2]+" "+arr[arr.length-1];
    }
    
    /**
     * Getter method for SilkAir flight date of departure
     * @return SilkAir flight date of departure
     */
    public String getArvDate() {
        return silkAir.getDate();
    }

    /**
     * Returns true if layover time is between 2 hours and 6 hours
     * @return true if layover time is between 2 hours and 6 hours
     */
    public boolean validLayover() {
        long z = layover();
        return (z >= 120 && z <= 360);
    }

    /**
     * Returns total flight time in minutes
     * @return Flight time in minutes
     */
    public long time() {//return in mins
        return (spiceJet.flightTime() + silkAir.flightTime()) / (1000 * 60) + layover();
    }

    /**
     * Returns total layover time in minutes
     * @return Layover time in minutes
     */
    public long layover() {
        long z = silkAir.depTime() - spiceJet.arvTime();
        z = (z < 0) ? z + (24 * 60 * 60 * 1000) : z;
        z = z / (1000 * 60);// in mins
        return z;
    }
}