
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;

/**
 * Maintains and creates logs for debugging purposes
 *
 * @author Ronit Ray
 */
public class LogManager {

    /**
     * Writes error/warning messages to a log file
     *
     * @param msg Message to be written
     * @param time Time of occurrence
     * @param fname Log file name
     */
    public static void writeToLog(String msg, long time, String fname) {
        FileWriter f;
        SimpleDateFormat fmt = new SimpleDateFormat("MMM-dd-yyyy HH:mm:ss");
        try {
            f = new FileWriter(fname, true);
            BufferedWriter bw = new BufferedWriter(f);
            PrintWriter pw = new PrintWriter(bw);
            pw.println(fmt.format(time) + "\t" + msg);
            pw.close();
            bw.close();
            f.close();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
}
