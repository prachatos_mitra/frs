
/**
 * Swing class for Confirmation / Ticket screen
 * @author Paramartha Saha
 */
public class ConfirmationScreen extends javax.swing.JFrame {

    private GraphicalUIManager mgr;
    /**
     * Constructor for ConfirmationScreen
     * @param ui Swing UI Manager
     */
    public ConfirmationScreen(GraphicalUIManager ui) {
        this.mgr = ui;
        this.setTitle("Booking confirmation");
        initComponents();
        setLocationRelativeTo(null);
        setResizable(false);
    }
    
    /**
     * Function to display Booking object in appropriate labels
     * @param book Booking Record of last booked flight
     */
    public void showBooking(Booking book) {
        jLabelArr.setText(book.getBooking().getArvDate());
        jLabelDep.setText(book.getBooking().getDepDate());
        jLabelFno1.setText(book.getBooking().getFno().substring(0, 8));
        jLabelFno2.setText(book.getBooking().getFno().substring(8));
        jLabelName.setText(book.getPassName().toUpperCase());
        jLabelPNR.setText(book.getPNR().toUpperCase());
        jLabelPassNo.setText(book.getPassNo());
        jLabelFrom1.setText(book.getBooking().getOrig());
        jLabelFrom2.setText(book.getBooking().getInter());
        jLabelTo1.setText(book.getBooking().getInter());              
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabelPNR = new javax.swing.JLabel();
        jLabelName = new javax.swing.JLabel();
        jLabelFno1 = new javax.swing.JLabel();
        jLabelDep = new javax.swing.JLabel();
        jLabelFrom1 = new javax.swing.JLabel();
        jLabelFno2 = new javax.swing.JLabel();
        jLabelArr = new javax.swing.JLabel();
        jLabelTo1 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabelPassNo = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabelFrom2 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(701, 380));
        setMinimumSize(new java.awt.Dimension(701, 380));
        setPreferredSize(new java.awt.Dimension(701, 380));
        getContentPane().setLayout(null);

        jButton1.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        jButton1.setText("Finish");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1);
        jButton1.setBounds(491, 320, 120, 29);

        jButton2.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        jButton2.setText("Book Another Flight");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton2);
        jButton2.setBounds(30, 320, 180, 29);

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ticket.png"))); // NOI18N
        getContentPane().add(jLabel1);
        jLabel1.setBounds(-60, -20, 300, 90);

        jLabel3.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        jLabel3.setText("Booking ID :");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(30, 60, 80, 19);

        jLabel4.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        jLabel4.setText("Name :");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(30, 90, 44, 19);

        jLabel5.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N
        jLabel5.setText("FLIGHT DETAILS");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(30, 170, 282, 19);

        jLabel6.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        jLabel6.setText("Flight No. :");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(30, 200, 69, 19);

        jLabel8.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        jLabel8.setText("Date of departure :");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(30, 230, 118, 19);

        jLabel9.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        jLabel9.setText("From:");
        getContentPane().add(jLabel9);
        jLabel9.setBounds(30, 260, 38, 19);

        jLabel10.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N
        jLabel10.setText("CONNECTING FLIGHT DETAILS");
        jLabel10.setFocusTraversalPolicyProvider(true);
        getContentPane().add(jLabel10);
        jLabel10.setBounds(380, 170, 298, 19);

        jLabel11.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        jLabel11.setText("Flight No. :");
        getContentPane().add(jLabel11);
        jLabel11.setBounds(380, 200, 69, 19);

        jLabel12.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        jLabel12.setText("Date of arrival:");
        getContentPane().add(jLabel12);
        jLabel12.setBounds(380, 230, 95, 19);

        jLabel13.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        jLabel13.setText("From:");
        getContentPane().add(jLabel13);
        jLabel13.setBounds(380, 260, 38, 19);

        jLabelPNR.setFont(new java.awt.Font("Serif", 0, 14)); // NOI18N
        jLabelPNR.setText("jLabel14");
        getContentPane().add(jLabelPNR);
        jLabelPNR.setBounds(170, 60, 132, 19);

        jLabelName.setFont(new java.awt.Font("Serif", 0, 14)); // NOI18N
        jLabelName.setText("jLabel14");
        getContentPane().add(jLabelName);
        jLabelName.setBounds(170, 90, 132, 19);

        jLabelFno1.setFont(new java.awt.Font("Serif", 0, 14)); // NOI18N
        jLabelFno1.setText("jLabel14");
        getContentPane().add(jLabelFno1);
        jLabelFno1.setBounds(180, 200, 132, 19);

        jLabelDep.setFont(new java.awt.Font("Serif", 0, 14)); // NOI18N
        jLabelDep.setText("jLabel14");
        getContentPane().add(jLabelDep);
        jLabelDep.setBounds(180, 230, 132, 19);

        jLabelFrom1.setFont(new java.awt.Font("Serif", 0, 14)); // NOI18N
        jLabelFrom1.setText("jLabel14");
        getContentPane().add(jLabelFrom1);
        jLabelFrom1.setBounds(180, 260, 132, 19);

        jLabelFno2.setFont(new java.awt.Font("Serif", 0, 14)); // NOI18N
        jLabelFno2.setText("jLabel14");
        getContentPane().add(jLabelFno2);
        jLabelFno2.setBounds(550, 200, 132, 19);
        jLabelFno2.getAccessibleContext().setAccessibleDescription("");

        jLabelArr.setFont(new java.awt.Font("Serif", 0, 14)); // NOI18N
        jLabelArr.setText("jLabel14");
        getContentPane().add(jLabelArr);
        jLabelArr.setBounds(550, 230, 132, 19);

        jLabelTo1.setFont(new java.awt.Font("Serif", 0, 14)); // NOI18N
        jLabelTo1.setText("jLabel14");
        getContentPane().add(jLabelTo1);
        jLabelTo1.setBounds(180, 290, 132, 19);

        jLabel14.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        jLabel14.setText("To:");
        getContentPane().add(jLabel14);
        jLabel14.setBounds(30, 290, 20, 19);

        jLabel15.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        jLabel15.setText("To:");
        getContentPane().add(jLabel15);
        jLabel15.setBounds(380, 290, 20, 19);

        jLabel16.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        jLabel16.setText("No. of passengers:");
        getContentPane().add(jLabel16);
        jLabel16.setBounds(30, 120, 112, 19);

        jLabelPassNo.setFont(new java.awt.Font("Serif", 0, 14)); // NOI18N
        jLabelPassNo.setText("jLabel14");
        getContentPane().add(jLabelPassNo);
        jLabelPassNo.setBounds(170, 120, 132, 19);

        jLabel17.setFont(new java.awt.Font("Serif", 0, 14)); // NOI18N
        jLabel17.setText("SINGAPORE");
        getContentPane().add(jLabel17);
        jLabel17.setBounds(550, 290, 132, 19);

        jLabelFrom2.setFont(new java.awt.Font("Serif", 0, 14)); // NOI18N
        jLabelFrom2.setText("jLabel18");
        getContentPane().add(jLabelFrom2);
        jLabelFrom2.setBounds(550, 260, 132, 19);

        jLabel21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/logo.png"))); // NOI18N
        getContentPane().add(jLabel21);
        jLabel21.setBounds(410, 10, 180, 140);

        jLabel2.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ConfirmationScreen.png"))); // NOI18N
        getContentPane().add(jLabel2);
        jLabel2.setBounds(0, 0, 710, 360);

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    /**
     * Method to go back to first screen
     * @param evt Event triggered on clicking Book Another Flight button
     */
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        mgr.showInputScreen();       // TODO add your handling code here:
    }//GEN-LAST:event_jButton2ActionPerformed
    
    /**
     * Method to turn off the program
     * @param evt Event triggered on clicking close
     */
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        mgr.displayOff();
    }//GEN-LAST:event_jButton1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelArr;
    private javax.swing.JLabel jLabelDep;
    private javax.swing.JLabel jLabelFno1;
    private javax.swing.JLabel jLabelFno2;
    private javax.swing.JLabel jLabelFrom1;
    private javax.swing.JLabel jLabelFrom2;
    private javax.swing.JLabel jLabelName;
    private javax.swing.JLabel jLabelPNR;
    private javax.swing.JLabel jLabelPassNo;
    private javax.swing.JLabel jLabelTo1;
    // End of variables declaration//GEN-END:variables
}
