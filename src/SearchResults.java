/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Paramartha Saha
 */
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.io.File;
import java.util.List;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;

/**
 * Swing class to display Search Results
 * @author Prachatos Mitra, Paramartha Saha
 */
class SearchResults extends JFrame {
    GraphicalUIManager mgr;
    private JPanel butGrpPanel = new JPanel(new GridLayout(0, 1, 0, 0)); 
    private JPanel butPanel= new JPanel(new GridLayout(1, 0, 200, 350));
    private JPanel topPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    private JRadioButton[] radioBtns;/* = new JRadioButton[10]*/;
    private JLabel jLabel1;
    private JButton jButton1 = new JButton("<html><B>Next</B></html>");
    private JButton jButton2 = new JButton("<html><B>Back</B><html>");
    private ButtonGroup btnGrp = new ButtonGroup();
    private JComboBox<String> jComboBox1;
    private List<ComboFlight> searchResult;
    private JScrollPane scrollPane;
   
    /**
     * Constructor for Search Results class
     * @param ui Swing UI Manager
     */
    public SearchResults(GraphicalUIManager ui) {
        this.mgr = ui;
        this.setTitle("Search Results");
        butPanel.add(jButton2);
        butPanel.add(jButton1);
        jComboBox1 = new javax.swing.JComboBox<>(); 
        jComboBox1.setPreferredSize(new Dimension(200, 20));
        //add list of intermediate cities
        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[]
                            { "ALL", "BENGALURU", "CHENNAI", "HYDERABAD", "KOLKATA" }));
        //add event handlers
        jButton1.addActionListener(this::selectFlight);
        jButton2.addActionListener(this::jButton2ActionPerformed); 
        jComboBox1.addActionListener(this::jComboBox1ActionPerformed);
        jLabel1 = new JLabel("Connecting city options:");
        topPanel.add(jLabel1, BorderLayout.WEST);
        topPanel.add(jComboBox1, BorderLayout.CENTER);
        jLabel1.setFont(new java.awt.Font(Font.SERIF, 1, 14));
        scrollPane = new JScrollPane();
        getContentPane().setBackground(new Color(118,156,191));
        butPanel.setBackground(new Color(118,156,191));
        topPanel.setBackground(new Color(118,156,191));
    }
     
    /**
     * Method to add all the generated search results to radio buttons
     * @param searchResults List containing search results
     */
    public void addSearchResult(java.util.List<ComboFlight> searchResults) {
        this.searchResult = searchResults;
        radioBtns = new JRadioButton[searchResults.size()];
        String spiceLogo = getClass().getResource("/images/SpiceJet_logo.jpg").toString();
        String silkLogo = getClass().getResource("/images/SilkAir_logo.jpg").toString();
        for (int i = 0; i < radioBtns.length; i++) {
            long timeMin = searchResults.get(i).time();
            long layTime = searchResults.get(i).layover();
            //parse time from millis to readable
            String totalTime = (timeMin/60)+" hrs, "+(timeMin%60)+" mins";
            String layoverTime = (layTime/60)+" hrs, "+(layTime%60)+" mins";
            /**
             * HTML inside radioButtons for handling style
             * Adds details for individual results to a button
             */
            radioBtns[i] = new JRadioButton("<html><div style='margin-left: 20px; background-color: #769CBF;'><div style='font-size: 15px; padding-bottom: 0px !important; width: 100% !important; background-color: #769CBF;'><b>" + searchResults.get(i).getOrig() + " → " + searchResults.get(i).getInter() + " → SINGAPORE</b></div>" +
                                            "<center><div style='font-size: 8px;'>Flight numbers: " + searchResults.get(i).getFno() + "</div></p></center>" +
                                            "<div style='font-size: 10px;'>Travel time : <b>"+ totalTime + "</b>, Layover of <b>" + layoverTime + "</b></div>" +
                                            "<div style='font-size: 10px; margin-top: 5px;';><img src=\"" + spiceLogo +"\" style='height: 5px !important;'>" +
                                            "&nbsp;&nbsp;(" + searchResults.get(i).getOrig() + ") " + searchResults.get(i).getDepDate() + " " + searchResults.get(i).getSpiceDep() + " (IST) - " + searchResults.get(i).getSpiceArv() + " (IST) "+
                                            "<div style='font-size: 10px; margin-top: 5px;';><img src=\"" + silkLogo +"\" style='height: 5px !important;'>" +
                                            "&nbsp;&nbsp;(" + searchResults.get(i).getInter() + ") " + searchResults.get(i).getInterDate() + " " + searchResults.get(i).getSilkDep() + " (IST) - " + searchResults.get(i).getArvDate() + " " + searchResults.get(i).getSilkArv() + " (SGT)"+
                                            "</div></html>");
            radioBtns[i].setFont(new java.awt.Font(Font.SERIF, 0, 13));
            radioBtns[i].setBackground(new Color(118,156,191));
            radioBtns[i].setAlignmentX(CENTER_ALIGNMENT);
            btnGrp.add(radioBtns[i]);
            butGrpPanel.add(radioBtns[i]);
            
        }
        radioBtns[0].setSelected(rootPaneCheckingEnabled);
        jComboBox1.setSelectedIndex(0);
        //handle display parameters
        jComboBox1.setFont(new java.awt.Font("SERIF", 0, 14));
        jButton1.setFont(new java.awt.Font("SERIF", 0, 14));
        jButton1.setFont(new java.awt.Font("SERIF", 0, 14));
        scrollPane.setViewportView(butGrpPanel);
        add(scrollPane, BorderLayout.CENTER);
        add(topPanel, BorderLayout.PAGE_START);
        add(butPanel,BorderLayout.PAGE_END);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        getContentPane().setBackground(new Color(118,156,191));
        setMaximumSize(new java.awt.Dimension(500,710));
        setMinimumSize(new java.awt.Dimension(500, 710));
        setPreferredSize(new java.awt.Dimension(500, 710));
        pack();
        
        setResizable(false);
        
        setVisible(true);
        
        setLocationRelativeTo(null);
    }
    
    /**
     * Utility method to set all radio Buttons visible
     */
    private void setVisibleAll() {
        for (int i = 0; i < radioBtns.length; ++i) {                   
            butGrpPanel.add(radioBtns[i]);
            radioBtns[i].setVisible(true);
        }
    }
    
    /**
     * Method to handle selection of specific intermediate cities
     * @param evt Event triggered by changing selected city
     */
    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) { 
        try {
            String selectedCity = jComboBox1.getSelectedItem().toString();
            if ("ALL".equals(selectedCity)) {                  
                setVisibleAll();
            } else {
                int firstBtn = -1; //keep track of how many buttons and top button
                for (int i = 0; i < radioBtns.length; ++i) {
                    if (!radioBtns[i].getText().contains(selectedCity)) {
                        butGrpPanel.remove(radioBtns[i]);
                        radioBtns[i].setVisible(false);
                    } else {
                        firstBtn = (firstBtn < 0)?i:firstBtn;
                        butGrpPanel.add(radioBtns[i]);
                        radioBtns[i].setVisible(true);
                    }
                }
                if (firstBtn == -1) {
                    //no flight found
                    JOptionPane.showMessageDialog(this, "No matching flights found connecting via " +
                                                    selectedCity, "Error", JOptionPane.ERROR_MESSAGE);           
                    jComboBox1.setSelectedItem((Object)"ALL");                    
                    setVisibleAll();
                }
                radioBtns[firstBtn].setSelected(true);
            }           
            butGrpPanel.repaint();
            pack();
        } catch(Exception e) {
            LogManager.writeToLog(e.getMessage(), System.currentTimeMillis(), "warning.log");                              
        }
    }
    
    /**
     * Function to load previous screens
     * @param evt Event triggered by clicking Back
     */
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {
        for (int i = 0; i < radioBtns.length; i++) {
            btnGrp.remove(radioBtns[i]);
            butGrpPanel.remove(radioBtns[i]);
        }
        mgr.showInputScreen();// TODO add your handling code here:
    }              
    
    /**
     * Function to select a specific flight for Booking
     * @param evt Event triggered by clicking Book flight
     */
    private void selectFlight(java.awt.event.ActionEvent evt) {        
        ComboFlight x = null;
        for (int i = 0; i < searchResult.size(); i++) {
            if (radioBtns[i].isSelected()) {
                x = searchResult.get(i);
                break;
            }
        }
        for (JRadioButton radioBtn : radioBtns) {
            btnGrp.remove(radioBtn);
            butGrpPanel.remove(radioBtn);
        }
        mgr.showBookingScreen(x);   
    } 
}