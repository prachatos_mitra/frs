/**
 * Designs fields to be written to the flight booking records
 * @author Rajarshi Basu, Ronit Ray
 */
public class Booking implements java.io.Serializable{
    private String pnr;
    private int passNo;
    private String passName;
    private ComboFlight flightBooked;
    /**
     * Initializes a flight record
     * @param pnr 6 digit UUID
     * @param passNo Number of passengers booked
     * @param passName Name of booking
     * @param f Flight booked
     */
    public Booking(String pnr,int passNo,String passName,ComboFlight f){
        this.pnr = pnr;
        this.passNo = passNo;
        this.passName = passName;
        flightBooked = f;
    }
    /**
     * Getter method to access UUID
     * @return 6 digit UUID
     */
    public String getPNR() {
        return pnr;
    }
    /**
     * Getter method to access number of passengers
     * @return Number of passengers
     */
    public String getPassNo() {
        return passNo + "";
    }
    /**
     * Getter method to access booking name
     * @return Passenger booking name
     */
    public String getPassName() {
        return passName;
    }
    /**
     * Getter method to access ComboFlight object
     * @return ComboFlight booked
     */
    public ComboFlight getBooking(){
        return flightBooked;
    }
}