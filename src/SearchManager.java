
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
/**
 * Implements Search logic for linking flights from the respective databases
 * @author Ronit Ray
 */
public class SearchManager {
    private FRSManager mgr;
    private DataManager dm;
    private List<ComboFlight> seatList;
    /**
     * Constructor to link with FRSManager
     * @param mgr Initial class constructor
     */
    public SearchManager(FRSManager mgr){
        this.mgr = mgr;
        this.dm = mgr.getDataMgr();
    }
    /**
     * Method to calculate valid connecting flights
     * @param dep Date of departure from originating city
     * @param orig Name of originating city
     * @param numPass Number of passengers booked
     * @return List of ComboFlight objects
     */
    public List<ComboFlight> search(Date dep,String orig,int numPass){
        seatList = dm.readSeatInfo();
        List<Flight> search = getValidSpiceFlight(dep, orig);
        List<ComboFlight> searchCo = getValidComboFlight(search, dep, numPass);
        searchCo.sort(Comparator.comparing(e->e.time()));
        return searchCo;
    }
    /**
     * Method to calculate valid SpiceJet flights
     * @param dep Date of departure from originating city
     * @param orig Name of originating city
     * @return List of SpiceJet objects
     */
    private List<Flight> getValidSpiceFlight(Date dep, String orig){
        List<Flight> spice = mgr.flightSpiceJet;
        List<Flight> search = new ArrayList<>();
        Calendar c = Calendar.getInstance();
        c.setTime(dep);
        int day = c.get(Calendar.DAY_OF_WEEK);
        for(Flight x:spice){
            boolean v = x.freq.equals("0") || (x.freq.indexOf(day+'0') != -1);
            boolean w = (dep.getTime() >= (x.getEffDate().getTime()) && (x.getToDate().getTime()) >= (dep.getTime()));
            if(x.orig.equals(orig) && w && v)
                search.add(x);
        }
        return search;
    }
    /**
     * Method to link valid SpiceJet and SilkAir flights
     * @param search
     * @param dep Date of departure from originating city
     * @param numPass Number of passengers booked
     * @return  List of SpiceJet objects
     */
    private List<ComboFlight> getValidComboFlight(List<Flight> search, Date dep, int numPass){
        List<Flight> silk = mgr.flightSilkAir;
        List<ComboFlight> searchCo = new ArrayList<>();
        DateFormat date = new SimpleDateFormat("dd MMM yyyy");
        Calendar c = Calendar.getInstance();
        c.setTime(dep);
        int day = c.get(Calendar.DAY_OF_WEEK);
        for(Flight x:search){
            ScheduledFlight spiceX = new ScheduledFlight(x,dep,numPass);
            for(Flight y:silk){
                Date arv = dep,inter = dep;
                Calendar d = Calendar.getInstance();
                d.setTime(arv);
                //check if depTime is valid
                if(x.arvTime.before(x.depTime) || y.arvTime.before(y.depTime) || x.arvTime.after(y.depTime)){
                    d.add(Calendar.DATE, 1);
                    try{
                        arv = date.parse(date.format(d.getTime()));
                    }
                    catch(ParseException e){
                        LogManager.writeToLog(e.getMessage(), System.currentTimeMillis(), "error.log");
                    }
                    day = d.get(Calendar.DAY_OF_WEEK);
                }
                //handle intermediate flight time
                if(x.arvTime.after(y.depTime)) inter=arv;
                if(!arv.equals(inter)){ 
                    day--;
                    day=(day==0)?7:day;
                }
                ScheduledFlight silkX = new ScheduledFlight(y,arv,numPass);
                ComboFlight comb = new ComboFlight(spiceX,silkX,inter, numPass);
                boolean v = y.freq.equals("0") || (y.freq.indexOf(day+'0') != -1);
                boolean w = (inter.getTime() >= (y.getEffDate().getTime()) && (y.getToDate().getTime()) >= (inter.getTime()));
                if(x.dest.equals(y.orig) && w && v && comb.validLayover() && checkFlightEnoughSeats(comb) && !excDates(inter,y))
                    searchCo.add(comb);
            }
        }
        return searchCo;
    }
    /**
     * Checks total number of passengers<=15
     * @param x ComboFlight to be checked
     * @return true if total number of passengers<=15
     */
    private boolean checkFlightEnoughSeats(ComboFlight x){
        if(seatList == null) return true;
        ScheduledFlight silk = x.getSilk();
        ScheduledFlight spice = x.getSpice();
        String ch1 = spice.getDate()+spice.getFno()+spice.orig;
        String ch2 = silk.getDate()+silk.getFno()+silk.orig;
        int sumSp = spice.getPass(),sumSi = silk.getPass();
        for(ComboFlight src:seatList){
            ScheduledFlight silkTest = src.getSilk(),spiceTest = src.getSpice();
            //combine data to string to check validity
            String s1 = spiceTest.getDate()+spiceTest.getFno()+spiceTest.orig;
            String s2 = silkTest.getDate()+silkTest.getFno()+silkTest.orig;
            if(s1.equals(ch1)) sumSp += spiceTest.getPass();
            if(s2.equals(ch2)) sumSi += silkTest.getPass();
        }
        return (sumSp <= ScheduledFlight.MAX_SEATS && sumSi <= ScheduledFlight.MAX_SEATS);
    }
    /**
     * Checks a particular SilkAir flight for excluded dates
     * @param d Excluded date
     * @param f SilkAir flight
     * @return true if flight is unavailable for Date d
     */
    private boolean excDates(Date d, Flight f){
        return f.excDateList.contains(d);
    }
}