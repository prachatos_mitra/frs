import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
/**
 * Swing class for Booking option
 * @author Paramartha Saha
 */
public class BookingScreen extends javax.swing.JFrame {
   
    GraphicalUIManager mgr;
    ComboFlight selectedFlight;
    
    /**
     * Constructor for Booking screen
     * @param ui Swing UI Manager
     */
    public BookingScreen(GraphicalUIManager ui) {
        this.mgr = ui;       
        this.setTitle("Book a flight");
        initComponents();
        setLocationRelativeTo(null);
        setResizable(false);
    }
    
    /**
     * Setter function to assign values to labels
     * @param selFlight Flight selected in SearchResults
     */
    public void setFlight(ComboFlight selFlight) {
        this.selectedFlight = selFlight;
        jLabelArr1.setText(selectedFlight.getSpiceArv());
        jLabelArr2.setText(selectedFlight.getSilkArv());
        jLabelDep1.setText(selectedFlight.getSpiceDep());
        jLabelDep2.setText(selectedFlight.getSilkDep());
        String Fno = selectedFlight.getFno();
        jLabelFno1.setText(Fno.substring(0,7));       
        jLabelFno2.setText(Fno.substring(8));
        jLabelFrom1.setText(selectedFlight.getOrig());
        jLabelTo1.setText(selectedFlight.getInter());
        jLabelFrom2.setText(selectedFlight.getInter());
        jLabelPass.setText(selectedFlight.getPassNo() + "");
        long x = selectedFlight.layover();
        jLabelLay.setText((x/60)+" hrs, "+(x%60)+" mins");
        jLabelDate1.setText(selectedFlight.getDepDate() + "");
        jLabelDate2.setText(selectedFlight.getArvDate() + "");
        long totTime = selectedFlight.time();
        jLabelTrav.setText((totTime/60) + " hrs, " + (totTime%60) + " mins");
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jTextField1 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabelPass = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabelFno1 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabelDate1 = new javax.swing.JLabel();
        jLabelFrom1 = new javax.swing.JLabel();
        jLabelTo1 = new javax.swing.JLabel();
        jLabelDep1 = new javax.swing.JLabel();
        jLabelArr1 = new javax.swing.JLabel();
        jLabelFno2 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        jLabelDate2 = new javax.swing.JLabel();
        jLabelFrom2 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        jLabelDep2 = new javax.swing.JLabel();
        jLabelArr2 = new javax.swing.JLabel();
        jLabelLay = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabelTrav = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(680, 715));
        setMinimumSize(new java.awt.Dimension(680, 715));
        setPreferredSize(new java.awt.Dimension(680, 715));
        getContentPane().setLayout(null);

        jLabel4.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        jLabel4.setText("Name :");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(29, 54, 106, 13);

        jLabel5.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        jLabel5.setText("No. Of Passengers :");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(29, 83, 121, 19);

        jLabel6.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        jLabel6.setText("Flight Number :");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(29, 155, 98, 19);

        jButton2.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        jButton2.setText("Confirm Booking");
        jButton2.setName(""); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bookingSelect(evt);
            }
        });
        getContentPane().add(jButton2);
        jButton2.setBounds(512, 654, 150, 23);

        jTextField1.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });
        getContentPane().add(jTextField1);
        jTextField1.setBounds(512, 49, 120, 23);

        jButton1.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        jButton1.setText("Back");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1);
        jButton1.setBounds(29, 654, 90, 29);

        jLabelPass.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jLabelPass.setText("jLabel1");
        jLabelPass.setFocusTraversalPolicyProvider(true);
        getContentPane().add(jLabelPass);
        jLabelPass.setBounds(512, 83, 120, 17);

        jLabel2.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        jLabel2.setText("Airline :");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(29, 183, 60, 19);

        jLabel3.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        jLabel3.setText("Date Of Journey :");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(29, 211, 130, 19);

        jLabel7.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        jLabel7.setText("From :");
        getContentPane().add(jLabel7);
        jLabel7.setBounds(29, 239, 50, 19);

        jLabel8.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        jLabel8.setText("To :");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(29, 267, 30, 19);

        jLabel9.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        jLabel9.setText("Departure :");
        getContentPane().add(jLabel9);
        jLabel9.setBounds(29, 295, 72, 19);

        jLabel10.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        jLabel10.setText("Arrival :");
        getContentPane().add(jLabel10);
        jLabel10.setBounds(29, 323, 53, 19);

        jLabel11.setFont(new java.awt.Font("Serif", 1, 16)); // NOI18N
        jLabel11.setText("CONNECTING FLIGHT DETAILS ");
        getContentPane().add(jLabel11);
        jLabel11.setBounds(29, 358, 270, 22);

        jLabel12.setFont(new java.awt.Font("Serif", 1, 16)); // NOI18N
        jLabel12.setText("PASSENGER DETAILS");
        getContentPane().add(jLabel12);
        jLabel12.setBounds(29, 11, 190, 20);

        jLabel13.setFont(new java.awt.Font("Serif", 1, 16)); // NOI18N
        jLabel13.setText("FLIGHT DETAILS");
        getContentPane().add(jLabel13);
        jLabel13.setBounds(29, 118, 160, 22);

        jLabel14.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        jLabel14.setText("Flight Number :");
        getContentPane().add(jLabel14);
        jLabel14.setBounds(29, 395, 110, 19);

        jLabel15.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        jLabel15.setText("Airline:");
        getContentPane().add(jLabel15);
        jLabel15.setBounds(29, 423, 60, 19);

        jLabel16.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        jLabel16.setText("Date Of Journey :");
        getContentPane().add(jLabel16);
        jLabel16.setBounds(29, 451, 112, 19);

        jLabel17.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        jLabel17.setText("From :");
        getContentPane().add(jLabel17);
        jLabel17.setBounds(29, 479, 50, 19);

        jLabel18.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        jLabel18.setText("To :");
        getContentPane().add(jLabel18);
        jLabel18.setBounds(29, 507, 40, 19);

        jLabel19.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        jLabel19.setText("Departure :");
        getContentPane().add(jLabel19);
        jLabel19.setBounds(29, 535, 80, 19);

        jLabel20.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        jLabel20.setText("Arrival :");
        getContentPane().add(jLabel20);
        jLabel20.setBounds(29, 563, 60, 19);

        jLabel22.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        jLabel22.setText("Layover Time :");
        getContentPane().add(jLabel22);
        jLabel22.setBounds(29, 591, 110, 19);

        jLabelFno1.setFont(new java.awt.Font("Serif", 0, 14)); // NOI18N
        jLabelFno1.setText("jLabel1");
        getContentPane().add(jLabelFno1);
        jLabelFno1.setBounds(512, 155, 120, 19);

        jLabel24.setFont(new java.awt.Font("Serif", 0, 14)); // NOI18N
        jLabel24.setText("SpiceJet");
        getContentPane().add(jLabel24);
        jLabel24.setBounds(512, 183, 120, 19);

        jLabelDate1.setFont(new java.awt.Font("Serif", 0, 14)); // NOI18N
        jLabelDate1.setText("jLabel1");
        getContentPane().add(jLabelDate1);
        jLabelDate1.setBounds(512, 211, 120, 19);

        jLabelFrom1.setFont(new java.awt.Font("Serif", 0, 14)); // NOI18N
        jLabelFrom1.setText("jLabel1");
        getContentPane().add(jLabelFrom1);
        jLabelFrom1.setBounds(512, 239, 120, 19);

        jLabelTo1.setFont(new java.awt.Font("Serif", 0, 14)); // NOI18N
        jLabelTo1.setText("jLabel1");
        getContentPane().add(jLabelTo1);
        jLabelTo1.setBounds(512, 268, 120, 14);

        jLabelDep1.setFont(new java.awt.Font("Serif", 0, 14)); // NOI18N
        jLabelDep1.setText("jLabel1");
        getContentPane().add(jLabelDep1);
        jLabelDep1.setBounds(512, 295, 120, 19);

        jLabelArr1.setFont(new java.awt.Font("Serif", 0, 14)); // NOI18N
        jLabelArr1.setText("jLabel1");
        getContentPane().add(jLabelArr1);
        jLabelArr1.setBounds(512, 323, 118, 19);

        jLabelFno2.setFont(new java.awt.Font("Serif", 0, 14)); // NOI18N
        jLabelFno2.setText("jLabel1");
        getContentPane().add(jLabelFno2);
        jLabelFno2.setBounds(512, 395, 118, 19);

        jLabel31.setFont(new java.awt.Font("Serif", 0, 14)); // NOI18N
        jLabel31.setText("SilkAir");
        getContentPane().add(jLabel31);
        jLabel31.setBounds(512, 423, 118, 19);

        jLabelDate2.setFont(new java.awt.Font("Serif", 0, 14)); // NOI18N
        jLabelDate2.setText("jLabel1");
        getContentPane().add(jLabelDate2);
        jLabelDate2.setBounds(512, 451, 118, 19);

        jLabelFrom2.setFont(new java.awt.Font("Serif", 0, 14)); // NOI18N
        jLabelFrom2.setText("jLabel1");
        getContentPane().add(jLabelFrom2);
        jLabelFrom2.setBounds(512, 479, 118, 19);

        jLabel34.setFont(new java.awt.Font("Serif", 0, 14)); // NOI18N
        jLabel34.setText("SINGAPORE");
        getContentPane().add(jLabel34);
        jLabel34.setBounds(512, 507, 118, 19);

        jLabelDep2.setFont(new java.awt.Font("Serif", 0, 14)); // NOI18N
        jLabelDep2.setText("jLabel1");
        getContentPane().add(jLabelDep2);
        jLabelDep2.setBounds(512, 535, 118, 19);

        jLabelArr2.setFont(new java.awt.Font("Serif", 0, 14)); // NOI18N
        jLabelArr2.setText("jLabel1");
        getContentPane().add(jLabelArr2);
        jLabelArr2.setBounds(512, 563, 118, 19);

        jLabelLay.setFont(new java.awt.Font("Serif", 0, 14)); // NOI18N
        jLabelLay.setText("jLabel1");
        getContentPane().add(jLabelLay);
        jLabelLay.setBounds(512, 591, 118, 19);

        jLabel23.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        jLabel23.setText("Total travel time:");
        getContentPane().add(jLabel23);
        jLabel23.setBounds(29, 619, 120, 19);

        jLabelTrav.setFont(new java.awt.Font("Serif", 0, 14)); // NOI18N
        jLabelTrav.setText("jLabel1");
        getContentPane().add(jLabelTrav);
        jLabelTrav.setBounds(512, 619, 118, 19);

        jLabel1.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/BookingScreen.png"))); // NOI18N
        getContentPane().add(jLabel1);
        jLabel1.setBounds(0, 0, 680, 690);

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    /**
     * Method to proceed to next screen
     * @param evt Triggering event
     */
    private void bookingSelect(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bookingSelect
        String name =  jTextField1.getText();
        //validate
        if (name.trim().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Name cannot be empty!", "Error", JOptionPane.ERROR_MESSAGE);          
        } else {
            mgr.showConfirmationScreen(selectedFlight, name); 
        }
    }//GEN-LAST:event_bookingSelect

    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
        

    }//GEN-LAST:event_jTextField1ActionPerformed
    
    /**
     * Method to go back to Search Results
     * @param evt Click event on Back button
     */
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        DateFormat df = new SimpleDateFormat("MMM dd yyyy");
        Date depDate = null;
        try {
            depDate = df.parse(selectedFlight.getDepDate());
        } catch (ParseException e) {
            LogManager.writeToLog(e.getMessage(), System.currentTimeMillis(), "error.log");
        }
        //recreate SearchResults form
        mgr.showSearchResults(selectedFlight.getOrig(), selectedFlight.getPassNo(), depDate);
    }//GEN-LAST:event_jButton1ActionPerformed
 
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelArr1;
    private javax.swing.JLabel jLabelArr2;
    private javax.swing.JLabel jLabelDate1;
    private javax.swing.JLabel jLabelDate2;
    private javax.swing.JLabel jLabelDep1;
    private javax.swing.JLabel jLabelDep2;
    private javax.swing.JLabel jLabelFno1;
    private javax.swing.JLabel jLabelFno2;
    private javax.swing.JLabel jLabelFrom1;
    private javax.swing.JLabel jLabelFrom2;
    private javax.swing.JLabel jLabelLay;
    private javax.swing.JLabel jLabelPass;
    private javax.swing.JLabel jLabelTo1;
    private javax.swing.JLabel jLabelTrav;
    private javax.swing.JTextField jTextField1;
    // End of variables declaration//GEN-END:variables
}