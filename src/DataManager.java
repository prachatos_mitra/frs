
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;
/**
 * Maintains, initializes flight records and updates booking records
 * @author Ronit Ray, Rajarshi Basu
 */
public class DataManager {
    private FRSManager mgr;
    private List<Flight> silkDB;
    private List<Flight> spiceDB;
    private final String silkFileName;
    private final String spiceFileName;
    /**
     * Loads flight databases
     * @param mgr Initial class constructor
     * @param spiceFile File containing SpiceJet flight details
     * @param silkFile File containing SilkAir flight details
     */
    public DataManager(FRSManager mgr, String spiceFile, String silkFile){
        this.mgr = mgr;
        this.spiceFileName = spiceFile;
        this.silkFileName = silkFile;
        spiceDB = getFile(spiceFileName);
        silkDB = getFile(silkFileName);
    }
    /**
     * Getter method for SpiceJet flight list
     * @return SpiceJet list
     */
    public List<Flight> getSpiceJet(){
        return spiceDB;
    }
    /**
     * Getter method for SilkAir flight list
     * @return SilkAir list
     */
    public List<Flight> getSilkAir(){
        return silkDB;
    }
    /**
     * Getter method for booked flight list
     * @return ComboFlight list
     */
    public List<ComboFlight> readSeatInfo() {
        return getList("flights.ser",1);
    }
    /**
     * Getter method for individual bookings list
     * @return Booking list
     */
    public List<Booking> readBooking(){
        return getList("flights.ser",0);
    }
    /**
     * Returns either all booked flights or individual booking details
     * @param s File name
     * @param x Parameter to determine type of return List
     * @return List<Booking> or List<ComboFlight>
     */
    private List getList(String s,int x){
        List<ComboFlight> ls = new ArrayList<>();
        List<Booking> book = new ArrayList<>();
        FileInputStream f = null;
        try{
            f = new FileInputStream(s);
            ObjectInputStream ois = new ObjectInputStream(f);
            book = (ArrayList<Booking>)ois.readObject(); //read from .ser file
            ois.close();
            f.close();
        }
        catch(FileNotFoundException e){
            LogManager.writeToLog(e.getMessage(), System.currentTimeMillis(), "warning.log");
        }
        catch(IOException | ClassNotFoundException e){
            LogManager.writeToLog(e.getMessage(), System.currentTimeMillis(), "error.log");
        }
        if(x == 0) return book;
        for(Booking b:book) ls.add(b.getBooking());
        return ls;
    }
    /**
     * Returns either SpiceJet or SilkAir flight details
     * @param st File name
     * @return List<Flight> of SpiceJet/SilkAir data
     */
    private List getFile(String st){
        List<Flight> l = new ArrayList<>();
        BufferedReader br;
        String s;
        try{
            br = new BufferedReader(new FileReader(st));
            while((s = br.readLine()) != null){
                String arr[] = s.split("\\|");
                if(arr.length<4 || arr[0].equals("Sector") || arr[0].equals("ORIGIN")) continue; //check valid
                if(st.equals(silkFileName))
                    l.add(new Flight(arr,1));
                else
                    l.add(new Flight(arr));
            }
        } catch (IOException e){
            LogManager.writeToLog(e.getMessage(), System.currentTimeMillis(), "error.log");
        }
        return l;
    }
}