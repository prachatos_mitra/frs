
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The Flight class creates Flight objects which are used to store the SpiceJet
 * and SilkAir flight data after parsing them.
 *
 * @author Ronit Ray, Prachatos Mitra
 */
public class Flight implements java.io.Serializable {

    String days[] = {"DAILY", "SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"};
    /**
     * Origin city
     */
    protected String orig;
    /**
     * Destination city
     */
    protected String dest;
    /**
     * Weekly frequency of flight
     */
    protected String freq;
    /**
     * Flight number
     */
    protected String fno;
    /**
     * Arrival time of flight
     */
    protected Date arvTime;
    /**
     * Departure time of flight
     */
    protected Date depTime;
    /**
     * Intermediate city (if present)
     */
    protected String via; //yet to be implemented
    private Date effFrom;
    private Date effTo;
    /**
     * Excluded dates list
     */
    protected List<Date> excDateList;

    /**
     * Parses and creates objects meant for storing SpiceJet flight details
     * 
     * @param arr Flight data to be parsed
     */
    public Flight(String arr[]) {
        DateFormat date = new SimpleDateFormat("dd MMM yyyy");
        DateFormat time = new SimpleDateFormat("hh:mm aa");
        excDateList = null;
        orig = arr[0].trim();
        dest = arr[1].trim();
        freq = getDays(arr[2]);
        fno = arr[3].trim();
        via = arr[6].trim();
        int x = arr[7].lastIndexOf(' ');
        String y = arr[7].substring(0, x) + " 20" + arr[7].substring(x + 1);
        try {
            depTime = time.parse(arr[4]);
            arvTime = time.parse(arr[5]);
            effFrom = date.parse(y);
            y = arr[8].substring(0, x) + " 20" + arr[8].substring(x + 1);
            effTo = date.parse(y);
        } catch (ParseException e) {
            LogManager.writeToLog(e.getMessage(), System.currentTimeMillis(), "error.log");
        }
    }

    /**
     * Parses and creates objects meant for storing SilkAir flight details
     *
     * @param arr Flight data to be parsed
     * @param i Parameter to support static polymorphism
     */
    public Flight(String arr[], int i) {
        DateFormat date = new SimpleDateFormat("dd MMM yyyy");
        excDateList = new ArrayList<>();
        String depArv[] = arr[3].split("/");
        orig = arr[0].substring(0, arr[0].indexOf(' ')).toUpperCase().trim();
        dest = "SINGAPORE";
        freq = getDays(arr[1]);
        fno = arr[2].trim();
        via = "-";
        depTime = convTime(depArv[0], 0);
        arvTime = convTime(depArv[1], 1);
        try {
            effFrom = date.parse("01 SEP 2016");
            effTo = date.parse("13 NOV 2016");
            if (arr.length > 4) handleRemarks(arr[4]);
        } catch (ParseException e) {
            LogManager.writeToLog(e.getMessage(), System.currentTimeMillis(), "error.log");
        }
    }

    /**
     * Initializes a Flight object from pre existing details. No parsing is
     * necessary
     *
     * @param f Flight object
     */
    public Flight(Flight f) {
        orig = f.orig;
        dest = f.dest;
        freq = f.freq;
        fno = f.fno;
        depTime = f.depTime;
        arvTime = f.arvTime;
        via = f.via;
        effFrom = f.effFrom;
        effTo = f.effTo;
        excDateList = f.excDateList;
    }
    
    /**
     * Function to handle remarks for SilkAir flights
     * @param remarks String containing remarks about specific entry
     * @throws ParseException Thrown if there is an error in Date parsing. Handled by Flight constructor 
     */
    private void handleRemarks(String remarks) throws ParseException {
        //Entry has remarks
        DateFormat df = new SimpleDateFormat("MMMdd yyyy");
        DateFormat df2 = new SimpleDateFormat("MMMd yyyy");
        //Every pattern has two formats: e.g. OCT 31 and OCT 3
        if (Pattern.matches(".*Disc\\.\\s[A-Z]{3}[0-9]{1,2}.*", remarks)) {
            //Regex patterns for Discontinued flights
            StringBuffer discDate;
            Matcher match = Pattern.compile("Disc.\\s([A-Z]{3}[0-9]{1,2})").matcher(remarks);
            if (match.find()) {
                discDate = new StringBuffer(match.group(1));
                discDate.append(" 2016");
                //Trying if format 1 works, else try 2.
                try {
                    effTo = df.parse(discDate.toString());
                } catch (ParseException e) {
                    effTo = df2.parse(discDate.toString()); //If this throws an error, handled by outer block
                }
            }
        } else if (Pattern.matches(".*Eff\\.\\s[A-Z]{3}[0-9]{1,2}.*", remarks)) {
            //Regex pattern for "Effective from"
            StringBuffer effDate;
            Matcher match = Pattern.compile("Eff.\\s([A-Z]{3}[0-9]{1,2})").matcher(remarks);
            if (match.find()) {
                effDate = new StringBuffer(match.group(1));
                effDate.append(" 2016");
                try {
                    effFrom = df.parse(effDate.toString());
                } catch (ParseException e) {
                    effFrom = df2.parse(effDate.toString());
                }
            }
        } else if (Pattern.matches(".*([A-Z]{3}[0-9]{1,2})\\s\\-\\s([A-Z]{3}[0-9]{1,2})", remarks)) {
            //Regex pattern for date ranges
            StringBuffer effDate, discDate;
            Matcher match = Pattern.compile("([A-Z]{3}[0-9]{1,2})\\s\\-\\s([A-Z]{3}[0-9]{1,2})").matcher(remarks);
            if (match.find()) {
                effDate = new StringBuffer(match.group(1));
                discDate = new StringBuffer(match.group(2));
                effDate.append(" 2016");
                try {
                    effFrom = df.parse(effDate.toString());
                } catch (ParseException e) {
                    effFrom = df2.parse(effDate.toString());
                }
                discDate.append(" 2016");
                try {
                    effTo = df.parse(discDate.toString());
                } catch (ParseException e) {
                    effTo = df2.parse(discDate.toString());
                }
            }
        }
        if (Pattern.matches(".*Exc\\.\\s[A-Z]{3}[0-9]{1,2}.*", remarks)) {
            //Regex pattern for Excluding dates
            String remarksRel;
            StringBuffer excDate;
            Matcher excExtract = Pattern.compile("Exc.\\s(.*)").matcher(remarks);
            if (excExtract.find()) {
                remarksRel = excExtract.group(1);
            } else {
                remarksRel = remarks;
            }
            Matcher match = Pattern.compile("([A-Z]{3}[0-9]{1,2})").matcher(remarksRel);
            while (match.find()) {
                excDate = new StringBuffer(match.group(1));
                excDate.append(" 2016");
                try {
                    excDateList.add(df.parse(excDate.toString()));
                } catch (ParseException e) {
                    excDateList.add(df.parse(excDate.toString()));
                }
            }
        }

    }
    /**
     * Getter method for FLight number
     *
     * @return Flight number
     */
    public String getFno() {
        return fno;
    }

    /**
     * Getter method for FLight date (effective from)
     *
     * @return FLight Date (effective from)
     */
    public Date getEffDate() {
        return effFrom;
    }

    /**
     * Getter method for FLight date (effective till)
     *
     * @return Flight date (effective till)
     */
    public Date getToDate() {
        return effTo;
    }

    /**
     * Calculates total flight time
     *
     * @return Flight time in milliseconds
     */
    public long flightTime() {
        long z = arvTime() - depTime();
        z = (z < 0) ? z + (24 * 60 * 60 * 1000) : z;
        return z;//in ms
    }

    /**
     * Getter method for flight arrival time
     *
     * @return Flight arrival time in milliseconds
     */
    public long arvTime() {
        return arvTime.getTime();
    }

    /**
     * Getter method for flight departure time
     *
     * @return Flight departure time in milliseconds
     */
    public long depTime() {
        return depTime.getTime();
    }

    /**
     * Parses validity of flight on particular days of the week
     *
     * @param s Data to be parsed
     * @return Valid days pattern
     */
    private String getDays(String s) {
        String arr[] = s.split(","), haha = "";
        if (arr[0].equals("DAILY") || arr.length == 7) {
            return "0";
        }
        s = s.toUpperCase();
        for (int i = 0; i < days.length; i++) {
            haha = (s.contains(days[i])) ? (haha + " " + i) : haha;
        }
        return haha.trim();
    }

    /**
     * Parses the SilkAir flight times to a suitable format
     *
     * @param s Data to be parsed
     * @param i Used to distinguish between SGT and IST
     * @return Parsed time as Date object
     */
    private Date convTime(String s, int i) {
        int x = s.indexOf('+');
        x = (x == -1) ? s.length() : x;
        String t1 = s.substring(0, x);
        DateFormat read = new SimpleDateFormat("HHmm");
        Date d = null;
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(read.parse(t1));
            if (i == 1) {
                c.add(Calendar.MINUTE, -(2 * 60 + 30));
            }
            d = read.parse(read.format(c.getTime()));
        } catch (ParseException e) {
            LogManager.writeToLog(e.getMessage(), System.currentTimeMillis(), "error.log");
        }
        return d;
    }
}
